# coveralls-ruby-demo

[Coveralls](https://coveralls.io/) demo project, using:

* [Ruby](https://www.ruby-lang.org/) — *Language*
* [Rspec](https://rspec.info/) — *Testing Library*
* [Simplecov](https://github.com/colszowka/simplecov) — *Code Coverage Library*

### How to run it:

```
bundle install
bundle exec rspec
```

## THIS IS A TEST

### Testing notes

1. Updating README to verify owner permissions.
