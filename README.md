# coveralls-ruby-demo

[Coveralls](https://coveralls.io/) demo project, using:

* [Ruby](https://www.ruby-lang.org/) — *Language*
* [Rspec](https://rspec.info/) — *Testing Library*
* [Simplecov](https://github.com/colszowka/simplecov) — *Code Coverage Library*

### How to run it:

```
bundle install
bundle exec rspec
```

## About Coveralls

[Coveralls](http://coveralls.io) is a web service that sends code coverage reports to a shared workspace so you and your team can track your project's code coverage over time. [Coveralls](http://coveralls.io) can also post PR comments and pass / fail checks to help you control your development workflow.

[Coveralls](http://coveralls.io) is language-, SCM- and CI-agnostic, but for this project we're using Ruby, GitHub and Gitlab CI.

## More

For more on integrating Coveralls with Gitlab CI, see the [Getting Started > Supported CIs](http://docs.coveralls.io/getting-started/supported-cis) section of [Coveralls documentation](http://docs.coveralls.io).
